#include <iostream>
#include <string>
#include <time.h>
#include <assert.h>
#include <vector>
#include "OrderedArray.h"
#include "UnorderedArray.h"

using namespace std;

void printArrays(UnorderedArray<int> unordered, OrderedArray<int> ordered)
{
	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
	cout << endl;
}

void main()
{
	srand(time(NULL));

	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (true)
	{
		printArrays(unordered, ordered);

		cout << "\n\nWhat do you want to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		cout << "Input: ";
		int choice = 0;
		while (choice < 1 || choice > 3)
		{
			cin >> choice;
		}

		if (choice == 1) // Remove element
		{
			cout << "\n\nEnter index to remove ";
			int input = -1;
			while (input < 0 || input > unordered.getSize() - 1)
			{
				cin >> input;
				if (input > unordered.getSize() - 1)
				{
					cout << "Index is more than the current size. Enter smaller index: ";
				}
			}

			cout << "Element removed: " << input << endl;
			unordered.remove(input);
			ordered.remove(input);

			printArrays(unordered, ordered);
		}

		else if (choice == 2) //Search index
		{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			cout << result << endl;
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
		}

		else if (choice == 3) //Expand array
		{
			cout << "\nInput size of expansion: ";
			int input = 0;
			while (input < 1)
			{
				cin >> input;
			}

			for (int i = 0; i < input; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "Arrays have been expanded." << endl;
			cout << "Unordered: ";
			printArrays(unordered, ordered);
		}

		system("pause");
		system("cls");
	}
 
	system("pause");
}
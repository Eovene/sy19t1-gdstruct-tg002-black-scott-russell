#pragma once
#include <assert.h>
#include <iostream>
using namespace std;

//Code derived from UnorderedArray.h
template<class T>
class Stack
{
public:
	Stack(int size, int growBy = 1) : mContainer(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mContainer = new T[mMaxSize];
			memset(mContainer, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	~Stack()
	{
		if (mContainer != NULL)
		{
			delete[] mContainer;
			mContainer = NULL;
		}
	}

	void push(int val)
	{
		assert(mContainer != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		mContainer[mNumElements] = val;
		mNumElements++;
		mCount++;
	}

	void printTop()
	{
		cout << "Stack Top: " << mContainer[mCount - 1] << endl;
	}

	void popTop() //Removes rightmost 
	{
		assert(mContainer != NULL);

		for (int i = mCount - 1; i < mMaxSize - 1; i++)
			mContainer[i] = mContainer[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}
	
	void printAndReset()
	{


		cout << "Stack Contents: ";
		if (mContainer[mCount - 1] == NULL)
		{
			return;
		}

		for (int i = mCount - 1; i > -1; i--)
		{
			cout << mContainer[i] << " ";
		}
		cout << endl;

		for (int i = 0; i < mMaxSize - 1; i++)
		{
			mContainer[i] = NULL;
		}
		mNumElements = 0;
		mCount = 0;
	}


private:
	T *mContainer;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;
	int mCount = 0;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mContainer, sizeof(T) * mMaxSize);

		delete[] mContainer;
		mContainer = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};


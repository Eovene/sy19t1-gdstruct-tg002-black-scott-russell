#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"
using namespace std;

int main()
{
	int size;
	cout << "Enter size of array: ";
	cin >> size;

	Stack<int> stack(size);
	Queue<int> queue(size);

	while (true)
	{
		system("cls");
		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;
		int choice = 0;

		while (choice < 1 || choice > 3)
		{
			cin >> choice;
		}

		if (choice == 1)//Push
		{
			int num;
			cout << "Enter number: ";
			cin >> num;
			stack.push(num);
			queue.push(num);

			cout << "\n\nTop of element sets" << endl;
			stack.printTop();
			queue.printTop();
		}

		else if (choice == 2)//Pop
		{
			stack.popTop();
			queue.popTop();
			cout << "You have popped the front elements";
		}

		else if (choice == 3)//Print
		{
			stack.printAndReset();
			queue.printAndReset();
		}
		system("pause");
	}

	system("pause");
	return 0;
}
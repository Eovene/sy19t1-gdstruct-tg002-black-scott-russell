#pragma once
#include <assert.h>
#include <iostream>
using namespace std;

//Code derived from UnorderedArray.h
template<class T>
class Queue
{
public:
	Queue(int size, int growBy = 1) : mContainer(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mContainer = new T[mMaxSize];
			memset(mContainer, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	~Queue()
	{
		if (mContainer != NULL)
		{
			delete[] mContainer;
			mContainer = NULL;
		}
	}

	void push(int val)
	{
		assert(mContainer != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		mContainer[mNumElements] = val;
		mNumElements++;
	}

	void printTop()
	{
		cout << "Queue Top: " << mContainer[0] << endl;
	}

	void popTop() //Removes leftmost
	{
		assert(mContainer != NULL);

		for (int i = 0; i < mMaxSize - 1; i++)
			mContainer[i] = mContainer[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	} 
	
	void printAndReset()
	{
		cout << "Queue Contents: ";
		if (mContainer[0] == NULL)
		{
			return;
		}

		for (int i = 0; i < mMaxSize - 1; i++)
		{
			if (mContainer[i] == NULL)
				break;
			cout << mContainer[i] << " ";
		}
		cout << endl;

		for (int i = 0; i < mMaxSize - 1; i++)
		{
			mContainer[i] = NULL;
		}
		mNumElements = 0;
	}

private:
	T *mContainer;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mContainer, sizeof(T) * mMaxSize);

		delete[] mContainer;
		mContainer = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};

